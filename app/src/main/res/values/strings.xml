<resources>
    <string name="app_name">ZenCrypt</string>
    <string name="encrypt_file">Encrypt File</string>
    <string name="encrypt_multiple">Encrypt Multiple</string>
    <string name="decrypt_multiple">Decrypt Multiple</string>
    <string name="decrypt_custom_file">Decrypt File</string>
    <string name="files_selected">Files Selected</string>
    <string name="encrypted">Encrypted</string>
    <string name="encrypt">Encrypt</string>
    <string name="decrypted">Decrypted</string>
    <string name="analyzer">Analyzer</string>
    <string name="about">About</string>
    <string name="settings">Settings</string>
    <string name="decrypt">Decrypt</string>
    <string name="share">Share</string>
    <string name="delete">Delete</string>
    <string name="rename">Rename</string>
    <string name="search">Search</string>
    <string name="actions">Actions</string>
    <string name="choose_action">Choose Action</string>
    <string name="search_description">Search files</string>
    <string name="please_enter_file_name_without_extension">Please enter the new file name without extension.</string>
    <string name="confirm_deletion">Confirm Deletion</string>
    <string name="password">Password</string>
    <string name="open">Open</string>
    <string name="file_name">File Name(s): </string>
    <string name="file_size">Total Size: </string>
    <string name="encrypting">Encrypting…</string>
    <string name="decrypting">Decrypting…</string>
    <string name="loading">Loading…</string>
    <string name="something_went_wrong_empty_password">Something went wrong. Maybe empty password?</string>
    <string name="maybe_wrong_password">Action Failed! Maybe wrong password?</string>
    <string name="operation_completed_successfully">Operation completed successfully.</string>
    <string name="support_development">Support Development</string>
    <string name="zencrypt_pro_is_already_purchased">ZenCrypt pro is already purchased in this account. Thanks for supporting the app!</string>
    <string name="pro_unlocked">Pro Unlocked</string>
    <string name="you_have_already_purchased">You have already purchased ZenCrypt Pro! You are awesome!</string>
    <string name="go_pro">Go Pro!</string>
    <string name="i_am_solo_developing">I am solo developing ZenCrypt. If you find this app useful, please consider unlocking the pro features.</string>
    <string name="google_play_billing_error">Google play billing error.</string>
    <string name="dark_theme">Dark Theme</string>
    <string name="choose_between_light_and_dark">Choose between light and dark theme.</string>
    <string name="zencrypt_pro_is_required">ZenCrypt pro is required to change this feature.</string>
    <string name="configuration">Configuration</string>
    <string name="also_copy_my_already_stored_files">Also copy my already stored files:</string>
    <string name="experimental_features">Experimental Features</string>
    <string name="enable_some_extra_experimental_features_keep_in_mind">Enable some experimental features. Keep in mind these may break functionality!</string>
    <string name="use_custom_directory">Use Custom Directory</string>
    <string name="change_zencrypts_working_directory">Change ZenCrypt\'s working directory, replacing the default /android/data location.</string>
    <string name="select_a_custom_directory_below">Select a custom directory below.</string>
    <string name="select_custom_directory">Select Custom Directory</string>
    <string name="custom_directory">Custom Directory</string>
    <string name="only_local_directories_are_supported">Only local directories are supported (such as SD cards and internal storage).</string>
    <string name="important_info">Important Info</string>
    <string name="while_selecting_a_custom_directory_offers_convenience">While selecting a custom directory offers convenience, keep in mind that it will reduce file processing speeds. This is because of inherent limitations in DocumentFile objects. Using this option also ensures that files will survive an app uninstall and remain on your storage.</string>
    <string name="iterations">Iterations</string>
    <string name="set_custom_iterations">Set Custom Iterations</string>
    <string name="change_the_default_number_of_iterations">Change the default number of iterations during the derivation of the symmetric key.</string>
    <string name="changing_the_number_of_iterations_will_not_affect_already_encrypted_files">Higher number equals increased resilience to password guessing attacks, but INCREASES processing times. Changing the number of iterations WILL NOT affect already encrypted files. This means that you WILL NOT be able to decrypt your currently encrypted files, unless you revert to the iteration number used for encryption.</string>
    <string name="enable_vibration">Enable Vibration</string>
    <string name="vibrate_after_every_action">Vibrate after every completed action (encryption/decryption).</string>
    <string name="delete_original_unencrypted">Delete Original Unencrypted</string>
    <string name="delete_original_file_after_encryption">Delete original file after encryption. Will ONLY work with files already stored within the app!</string>
    <string name="delete_unencrypted_files">Delete Unencrypted Files</string>
    <string name="delete_unencrypted_on_app_exit">Delete all stored unencrypted files on application exit. Requires app restart.</string>
    <string name="enabling_this_option_requires_restart">Enabling this option will delete all unencrypted files on each application exit.
                                                        \nBe cautious!
                                                        \nApplication restart is required.</string>
    <string name="restart_required">Application restart is required.</string>
    <string name="warning">Warning</string>
    <string name="file_sorting">File Sorting</string>
    <string name="choose_how_you_want_to_sort_your_files">Choose how you want to sort your stored files.</string>
    <string name="file_display_view">File Display View</string>
    <string name="choose_how_you_want_to_display_file_list_items">Choose how you want to display file list items.</string>
    <string name="export_encrypted_files">Export Encrypted Files</string>
    <string name="import_encrypted_files">Import Encrypted Files</string>
    <string name="import_all_encrypted_files_from_selected_location">Import all encrypted files from a selected location, straight to the app.</string>
    <string name="export_all_encrypted_files_on_selected_location">Export all encrypted files on a selected location, so that they can persist an app uninstall and remain on your storage.</string>
    <string name="encrypted_files_export">Encrypted Files Export</string>
    <string name="encrypted_files_import">Encrypted Files Import</string>
    <string name="file_operations">File Operations</string>
    <string name="delete_all_stored_files">Delete Stored Files</string>
    <string name="delete_files_stored_within_the_app">This will delete ALL files stored inside ZenCrypt\'s storage!</string>
    <string name="selected_location">Selected Folder:</string>
    <string name="this_process_can_take_some_time_to_export_import">This process may take some time.\nPlease don\'t close the app!</string>
    <string name="file_export_completed">File export completed!</string>
    <string name="file_import_completed">File import completed!</string>
    <string name="encrypted_file_extension">Encrypted File Extension</string>
    <string name="file_extension">File extension</string>
    <string name="are_you_sure_you_typed_the_correct_file_extension">Are you sure you typed correctly the file extension?</string>
    <string name="fingerprint_auth">Fingerprint Authentication</string>
    <string name="enable_fingerprint_auth">Enable Fingerprint Auth</string>
    <string name="enabling_fingerprint_auth">Enabling fingerprint authentication requires setting a custom password below!</string>
    <string name="set_a_custom_password_below">Set a custom password below.</string>
    <string name="fingerprint">Fingerprint</string>
    <string name="fingerprint_pro">Fingerprint (PRO)</string>
    <string name="fingerprint_is_not_enabled">Fingerprint is not enabled in settings!</string>
    <string name="set_custom_password">Set Custom Password</string>
    <string name="any_password_is_valid">Any password is valid. Password keys are always stored on the device.</string>
    <string name="enter_a_password">Enter a password…</string>
    <string name="empty_password">Empty Password!</string>
    <string name="this_file_manager_is_not_supported">This file manager isn\'t supported. Please try another one.</string>
    <string name="something_went_wrong">Something went wrong.</string>
    <string name="secure_storage_keys_not_found">Secure Storage Keys not Found!</string>
    <string name="password_set">Password Set!</string>
    <string name="about_zencrypt">About ZenCrypt</string>
    <string name="rate_zencrypt">Rate ZenCrypt</string>
    <string name="if_you_like_the_app">If you like the app, please let me know!</string>
    <string name="contact_me">Contact me</string>
    <string name="send_me_an_email">Send me an email regarding ZenCrypt. I will try and assist you as best as possible.</string>
    <string name="zencrypt_changelog_and_libraries">ZenCrypt changelog, information and open-source libraries.</string>
    <string name="source_code">Source Code</string>
    <string name="encrypted_path">Android/data/com.zestas.cryptmyfiles/files/encrypted</string>
    <string name="decrypted_path">Android/data/com.zestas.cryptmyfiles/files/decrypted</string>
    <string name="generate_random">Generate Pseudo Random</string>
    <string name="using_nulab_library">Powered by Nulab\'s zxcvbn4j library!</string>
    <string name="no_warning">No warning found.</string>
    <string name="file_received">File Received</string>
    <string name="please_choose_what_you_want_to_do">Please choose what you want to do with the received file.</string>
    <string name="randomize_file_name">Randomize File Name(s)</string>
    <string name="zencrypt_is_now_open_source">Zencrypt is now fully open-source. You can view the code on GitLab!</string>
    <string name="empty_list_view_placeholder">It looks like you have no files yet…\nClick \"+\" to start encrypting!</string>
    <string name="zencrypt_description">Copyright © Zestas Orestis. Welcome to ZenCrypt. With this app, you can safely encrypt files, using a secure and efficient library.
        Zencrypt offers AES-256 encryption algorithm, CBC mode of operations, block padding with PKCS7,
        computationally secure random salt (of cipher block size), password stretching with PBKDF2, random IV generation on each encryption (16 bytes),
        and password analysis for strength, crack times, weakness, etc using nulab\'s zxcvbn4j library. Finally, the application does not leak (tested with LeakCanary),
        and has no internet connection permission (except for google play billing library, which is required for purchasing pro version). You can check the encryption library\'s
        source code below (EasyCrypt, Priyank Vasa on Github).</string>

    <string name="zencrypt_changelog"><![CDATA[&#9874; ZenCrypt v4.7<br/>
        <br/>&#10148; Added Spanish translation. Thanks to Alvaro Orellana (@Mwazoski)!
        <br/>&#10148; Build tools and libraries updated.
        <br/><br/>&#9874; ZenCrypt v4.6<br/>
        <br/>&#10148; Fixed a lot of memory leaks that were introduced in the latest versions, which should improve performance. This was a major issue, and I apologize for the inconvenience. ZenCrypt is once again completely leak free.
        <br/>&#10148; Code optimizations, refactoring and cleanup.
        <br/>&#10148; Fixed a rare bug where the app would crash when showing the rename/delete dialog.
        <br/>&#10148; Kotlin updated to 1.9.22.
        <br/>&#10148; Build tools and libraries updated.]]></string>
</resources>